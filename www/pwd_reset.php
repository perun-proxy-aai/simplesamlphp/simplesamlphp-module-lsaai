<?php

declare(strict_types=1);

use SimpleSAML\Configuration;
use SimpleSAML\Module\perun\AdapterRpc;
use SimpleSAML\XHTML\Template;

$config = Configuration::getInstance();

$t = new Template($config, 'lsaai:pwd_reset-tpl.php');
$t->show();
