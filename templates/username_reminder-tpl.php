<?php

declare(strict_types=1);

use SimpleSAML\Configuration;
use SimpleSAML\Logger;
use SimpleSAML\Module\lsaai\TemplateHelper;

$config = Configuration::getInstance();

if (!isset($_POST['usernameReminderOk'])) {
    $_POST['usernameReminderOk'] = false;
}

/*
 * Support the htmlinject hook, which allows modules to change header, pre and post body on all pages.
 */
$this->data['htmlinject'] = [
    'htmlContentPre' => [],
    'htmlContentPost' => [],
    'htmlContentHead' => [],
];

$jquery = [];
if (array_key_exists('jquery', $this->data)) {
    $jquery = $this->data['jquery'];
}

if (array_key_exists('pageid', $this->data)) {
    $hookinfo = [
        'pre' => &$this->data['htmlinject']['htmlContentPre'],
        'post' => &$this->data['htmlinject']['htmlContentPost'],
        'head' => &$this->data['htmlinject']['htmlContentHead'],
        'jquery' => &$jquery,
        'page' => $this->data['pageid'],
    ];

    SimpleSAML\Module::callHooks('htmlinject', $hookinfo);
}

header('X-Frame-Options: SAMEORIGIN');

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0"/>
    <script type="text/javascript" src="/<?php
    echo $this->data['baseurlpath']; ?>resources/script.js"></script>
    <title>LS Username Login</title>

    <link rel="stylesheet" type="text/css" href="/<?php
    echo $this->data['baseurlpath']; ?>resources/default.css"/>
    <link rel="icon" type="image/icon" href="/<?php
    echo $this->data['baseurlpath']; ?>resources/icons/favicon.ico"/>

    <?php

    if (!empty($this->data['htmlinject']['htmlContentHead'])) {
        foreach ($this->data['htmlinject']['htmlContentHead'] as $c) {
            echo $c;
        }
    }

    if ($this->isLanguageRTL()) {
        ?>
        <link rel="stylesheet" type="text/css" href="/<?php
        echo $this->data['baseurlpath']; ?>resources/default-rtl.css"/>
        <?php
    }
    ?>

    <link rel="stylesheet" type="text/css" href="<?php
    echo SimpleSAML\Module::getModuleUrl('lsaai/res/bootstrap/css/bootstrap.min.css'); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php
    echo SimpleSAML\Module::getModuleUrl('lsaai/res/css/lsaai.css'); ?>"/>

    <meta name="robots" content="noindex, nofollow"/>


    <?php
    if (array_key_exists('head', $this->data)) {
        echo '<!-- head -->' . $this->data['head'] . '<!-- /head -->';
    }
    ?>
</head>
<?php
$onLoad = '';
if (array_key_exists('autofocus', $this->data)) {
    $onLoad .= 'SimpleSAML_focus(\'' . $this->data['autofocus'] . '\');';
}
if (isset($this->data['onLoad'])) {
    $onLoad .= $this->data['onLoad'];
}

if ('' !== $onLoad) {
    $onLoad = ' onload="' . $onLoad . '"';
}
?>
<body<?php
echo $onLoad; ?>>


<div id="wrap">

    <div id="content" class="content">
        <div class="row pl-0 pr-0">
            <div class="col-md-6 col-md-offset-3 logo-wrap col-align--center">
                <img src="<?php
                echo SimpleSAML\Module::getModuleUrl('lsaai/res/img/ls_logo.png'); ?>" alt="LS Username Login Logo">
            </div>
            <div class="col-md-6 col-md-offset-3">
                <?php
                echo '<h1> ' . $this->t('{lsaai:username_reminder:header}') . '</h1>';
                $userEmail = '';
                if (isset($_POST['email'])) {
                    $userEmail = $_POST['email'];
                    try {
                        if (!$_POST['usernameReminderOk']) {
                            TemplateHelper::sendUsernameReminder($userEmail);
                            $_POST['usernameReminderOk'] = true;
                            unset($_POST['email']);
                        } ?>
                        <div class="alert alert-success">
                            <p>
                                <span class="glyphicon glyphicon-exclamation-sign"
                                      style="float:left; font-size: 38px; margin-right: 10px;"></span>
                                <strong><?php
                                    echo $this->t('{lsaai:username_reminder:ok_header}'); ?></strong>
                            </p>
                            <p><?php
                                echo $this->t('{lsaai:username_reminder:ok_text}'); ?></p>
                        </div>

                        <?php
                    } catch (\Exception $exception) {
                        Logger::error('username_reminder-tpl.php - ' . $exception->getMessage());
                        $emailAddress = $config->getString('technicalcontact_email');
                        if (!str_starts_with('mailto:', $emailAddress)) {
                            $emailAddress = 'mailto:' . $emailAddress;
                        } ?>
                        <div class="alert alert-danger">
                            <span class="glyphicon glyphicon-exclamation-sign"
                                  style="float:left; font-size: 38px; margin-right: 10px;"></span>
                            <strong><?php
                                echo $this->t('{lsaai:username_reminder:err_header}'); ?></strong>
                            <p><?php
                                echo $this->t('{lsaai:username_reminder:err_text_part1}'); ?>
                                <a href="<?php
                                  echo $emailAddress; ?>">
                                  <?php echo $this->t('{lsaai:username_reminder:support}'); ?></a>.
                                  <?php echo $this->t('{lsaai:username_reminder:err_text_part2}'); ?>
                            </p>
                        </div>

                        <?php
                    }
                }

                if (!$_POST['usernameReminderOk']) {
                    ?>

                    <p><?php
                        echo $this->t('{lsaai:username_reminder:text}'); ?></p>

                    <br>

                    <form action="" method="post" name="username_reminder" class="form-horizontal">
                        <div class="form-group">
                            <label class="sr-only" for="inlineFormInputGroup"><?php
                                echo $this->t('{lsaai:username_reminder:email}'); ?></label>
                            <div class="input-group mb-2">
                            <span class="input-group-addon">
                                    <span class=" glyphicon glyphicon-envelope" id="basic-addon1"></span>
                            </span>
                                <input id="email" name="email" class="form-control" value="<?php
                                echo $userEmail; ?>" placeholder="Email address" aria-describedby="basic-addon1"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-success btn-block" type="submit">
                                <?php
                                echo $this->t('{lsaai:username_reminder:submit}'); ?>
                            </button>
                        </div>
                      <div class="form-group text-center">
                        <a class="btn btn-link" href="<?php
                        echo SimpleSAML\Module::getModuleURL("lsaai/pwd_reset.php");?>">
                            <?php
                            echo $this->t('{lsaai:username_reminder:button_forgotten_password}')?>
                        </a>
                      </div>
                    </form>

                    <?php
                }
                ?>
            </div>

<?php

$this->includeAtTemplateBase('includes/footer.php');
