<?php

namespace SimpleSAML\Module\lsaai;

use SimpleSAML;
use SimpleSAML\Configuration;
use SimpleSAML\Error\Exception;
use SimpleSAML\Logger;
use SimpleSAML\Module\perun\AdapterRpc;
use SimpleSAML_XHTML_Template;

class TemplateHelper
{
    public const LANG_EN = 'en';
    public const PWD_RESET = 'pwd_reset';
    public const PWDRESET_LS_USERNAME_LOGIN_ENTITY_ID = 'ls_username_login_entity_id';
    public const PWDRESET_LS_USERNAME_LOGIN_SCOPE = 'ls_username_login_scope';
    public const PWDRESET_VO_SHORT_NAME = 'vo_short_name';
    public const PWDRESET_PERUN_NAMESPACE = 'perun_namespace';
    public const PWDRESET_PERUN_URL = 'perun_url';
    public const PWDRESET_PERUN_EMAIL_ATTR = 'perun_email_attr';
    public const CONFIG_FILE_NAME = 'module_lsaai.php';

    public static function sendPasswordResetEmail($userName)
    {
        $rpcAdapter = new AdapterRpc();

        $conf = Configuration::getConfig(TemplateHelper::CONFIG_FILE_NAME);
        $pwdConf = $conf->getConfigItem(TemplateHelper::PWD_RESET);
        $lsUsernameLoginEntityId = $pwdConf->getString(TemplateHelper::PWDRESET_LS_USERNAME_LOGIN_ENTITY_ID);
        $lsUsernameLoginScope = $pwdConf->getString(TemplateHelper::PWDRESET_LS_USERNAME_LOGIN_SCOPE);
        $voShortName = $pwdConf->getString(TemplateHelper::PWDRESET_VO_SHORT_NAME);
        $perunNamespace = $pwdConf->getString(TemplateHelper::PWDRESET_PERUN_NAMESPACE);
        $perunUrl = $pwdConf->getString(TemplateHelper::PWDRESET_PERUN_URL);
        $emailAttr = $pwdConf->getString(TemplateHelper::PWDRESET_PERUN_EMAIL_ATTR);

        $userName = trim($userName);
        Logger::debug(print_r($userName, true));

        $uid = [$userName . $lsUsernameLoginScope];
        try {
            $user = $rpcAdapter->getPerunUser($lsUsernameLoginEntityId, $uid);
            Logger::debug(print_r($user, true));
        } catch (Exception $ex) {
            throw new Exception($ex);
        }
        if (null === $user) {
            throw new Exception('There is no Lifescience RI user with username: ' . $userName);
        }

        $vo = $rpcAdapter->getVoByShortName($voShortName);
        $member = $rpcAdapter->getMemberByUser($user, $vo);

        $connector = $rpcAdapter->getConnector();

        $response = $connector->post(
            'membersManager',
            'sendPasswordResetLinkEmail',
            [
                'member' => $member->getId(),
                'namespace' => $perunNamespace,
                'url' => $perunUrl,
                'emailAttributeURN' => $emailAttr,
                'language' => TemplateHelper::LANG_EN,
            ]
        );

        Logger::debug(print_r($response, true));
    }

    public static function sendUsernameReminder($email)
    {
        $rpcAdapter = new AdapterRpc();

        $conf = Configuration::getConfig(TemplateHelper::CONFIG_FILE_NAME);
        $pwdConf = $conf->getConfigItem(TemplateHelper::PWD_RESET);
        $voShortName = $pwdConf->getString(TemplateHelper::PWDRESET_VO_SHORT_NAME);
        $perunNamespace = $pwdConf->getString(TemplateHelper::PWDRESET_PERUN_NAMESPACE);
        $emailAttr = $pwdConf->getString(TemplateHelper::PWDRESET_PERUN_EMAIL_ATTR);

        $userName = trim($email);
        Logger::debug(print_r($email, true));

        try {
            $user = $rpcAdapter->getPerunUserByEmail($emailAttr, $email);
            Logger::debug(print_r($user, true));
        } catch (Exception $ex) {
            throw new Exception($ex);
        }
        if (null === $user) {
            throw new Exception('There is no Lifescience RI user with username: ' . $userName);
        }

        $vo = $rpcAdapter->getVoByShortName($voShortName);
        $member = $rpcAdapter->getMemberByUser($user, $vo);

        $connector = $rpcAdapter->getConnector();

        $response = $connector->post(
            'membersManager',
            'sendUsernameReminderEmail',
            [
                'member' => $member->getId(),
                'namespace' => $perunNamespace,
                'emailAttributeURN' => $emailAttr,
                'language' => TemplateHelper::LANG_EN,
            ]
        );

        Logger::debug(print_r($response, true));
    }

    /**
     * Recursive attribute array listing function.
     *
     * @param SimpleSAML_XHTML_Template $t Template object
     * @param array $attributes Attributes to be presented
     * @param string $nameParent Name of parent element
     *
     * @return string HTML representation of the attributes
     */
    public static function presentAttributes($t, $attributes, $nameParent)
    {
        $i = 0;
        $summary = 'summary="' . $t->t('{consent:consent:table_summary}') . '"';

        if (strlen($nameParent) > 0) {
            $parentStr = strtolower($nameParent) . '_';
            $str = '<table class="table attributes" ' . $summary . '>';
        } else {
            $parentStr = '';
            $str = '<table id="table_with_attributes" class="table attributes" ' . $summary . '>';
            $str .= "\n" . '<caption>' . $t->t('{consent:consent:table_caption}') .
                '</caption>';
        }

        foreach ($attributes as $name => $value) {
            $nameraw = $name;
            $name = $t->getAttributeTranslation($parentStr . $nameraw);

            if (preg_match('/^child_/', $nameraw)) {
                // insert child table
                $parentName = preg_replace('/^child_/', '', $nameraw);
                foreach ($value as $child) {
                    $str .= "\n" . '<tr class="odd"><td style="padding: 2em">' .
                        self::presentAttributes($t, $child, $parentName) . '</td></tr>';
                }
            } else {
                // insert values directly

                $str .= "\n" . '<tr><td><span class="attrname">' . htmlspecialchars($name) . '</span>';

                $isHidden = in_array($nameraw, $t->data['hiddenAttributes'], true);
                if ($isHidden) {
                    $hiddenId = SimpleSAML\Utils\Random::generateID();

                    $str .= '<div class="attrvalue" style="display: none;" id="hidden_' . $hiddenId . '">';
                } else {
                    $str .= '<div class="attrvalue">';
                }

                if (sizeof($value) > 1) {
                    // we hawe several values
                    $str .= '<ul>';
                    foreach ($value as $listitem) {
                        if ('jpegPhoto' === $nameraw) {
                            $str .= '<li><img src="data:image/jpeg;base64,' .
                                htmlspecialchars($listitem) .
                                '" alt="User photo" /></li>';
                        } else {
                            $str .= '<li>' . htmlspecialchars($listitem) . '</li>';
                        }
                    }
                    $str .= '</ul>';
                } elseif (isset($value[0])) {
                    // we hawe only one value
                    if ('jpegPhoto' === $nameraw) {
                        $str .= '<img src="data:image/jpeg;base64,' .
                            htmlspecialchars($value[0]) .
                            '" alt="User photo" />';
                    } else {
                        $str .= htmlspecialchars($value[0]);
                    }
                } // end of if multivalue
                $str .= '</div>';

                if ($isHidden) {
                    $str .= '<div class="attrvalue consent_showattribute" id="visible_' . $hiddenId . '">';
                    $str .= '... ';
                    $str .= '<a class="consent_showattributelink" href="javascript:SimpleSAML_show(\'hidden_' .
                        $hiddenId;
                    $str .= '\'); SimpleSAML_hide(\'visible_' . $hiddenId . '\');">';
                    $str .= $t->t('{consent:consent:show_attribute}');
                    $str .= '</a>';
                    $str .= '</div>';
                }

                $str .= '</td></tr>';
            }   // end else: not child table
        }   // end foreach
        $str .= isset($attributes) ? '</table>' : '';

        return $str;
    }

    /**
     * Write out one or more INPUT elements for the given name-value pair.
     *
     * If the value is a string, this function will write a single INPUT element.
     * If the value is an array, it will write multiple INPUT elements to recreate the array.
     *
     * @param string $name the name of the element
     * @param string|array $value the value of the element
     */
    public static function printItem($name, $value)
    {
        assert('is_string($name)');
        assert('is_string($value) || is_array($value)');

        if (is_string($value)) {
            echo '<input type="hidden" name="' . htmlspecialchars($name) . '" value="' . htmlspecialchars($value) .
                '" />';

            return;
        }

        // This is an array...
        foreach ($value as $index => $item) {
            self::printItem($name . '[' . $index . ']', $item);
        }
    }
}
